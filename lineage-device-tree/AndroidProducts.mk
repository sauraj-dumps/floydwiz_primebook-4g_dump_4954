#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_Primebook-4G.mk

COMMON_LUNCH_CHOICES := \
    lineage_Primebook-4G-user \
    lineage_Primebook-4G-userdebug \
    lineage_Primebook-4G-eng
